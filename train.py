import argparse
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
from pathlib import Path
from tqdm import tqdm

def load_tensors(fen_filepath='/fens',eval_filepath='/evals', num_tensors=1000, start_id=0):
    """Loads tensors from paths. Assumes id-numid.pt naming"""
    fens,evals = {},{}
    fen_path = Path(fen_filepath)
    eval_path = Path(eval_filepath)
    for idx in tqdm(range(start_id,num_tensors),"From disk"):
        fen_filepath = fen_path / Path(f'id-{idx}.pt')
        eval_filepath = eval_path / Path(f'id-{idx}.pt')
        fens[idx] = torch.load(fen_filepath)
        evals[idx] = torch.load(eval_filepath)
    return fens,evals


class MyDataset(torch.utils.data.Dataset):
    def __init__(self,inputs,labels):
        self.inputs = inputs
        self.labels = labels

    def __getitem__(self, idx):
        return self.inputs[idx],self.labels[idx]

    def __len__(self):
        return len(self.inputs)

class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(8, 16, kernel_size=3)
        self.conv2 = nn.Conv2d(16, 32, 3)
        self.fc1 = nn.Linear(8*8*8,200)#TODO
        self.fc2 = nn.Linear(200, 1)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 8*8*8)#TODO
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        # here real number
        return x

def train(epoch, model, optimizer, criterion, train_loader, device):
    model.train()
    for idx, (inputs,labels) in enumerate(train_loader):
        inputs, labels = inputs.to(device), labels.float().to(device)

        optimizer.zero_grad()
        output = model(inputs)
        print(output)
        print(labels)
        loss = criterion(output, labels)
        loss.backward()
        optimizer.step()
        if idx % 100 == 0:
            print(f'{epoch=:<5} {idx/len(inputs):<5}% Loss:{loss.item():>5.4f}')

def test(model, optimizer, criterion, test_loader, device):
    model.eval()
    for inputs, labels in test_loader:
        inputs, labels = inputs.to(device), labels.float().to(device)
        output = model(inputs)
        test_loss = criterion(output, labels)
        print(f'Loss:{test_loss.item():>5.4f}')


def main(n_tensors,path,fen_filepath='./fens',eval_filepath='./evals'):
    fen_path = path / Path(fen_filepath)
    eval_path = path / Path(eval_filepath)
    train_tensors = math.floor(int(0.8*n_tensors))
    test_tensors = n_tensors - train_tensors
    train_fens,train_evals = load_tensors(fen_path,eval_path,num_tensors=train_tensors)
    test_fens,test_evals = load_tensors(fen_path,eval_path,num_tensors=test_tensors)
    train_dataset = MyDataset(train_fens, train_evals)
    test_dataset = MyDataset(test_fens, test_evals)
    train_loader = torch.utils.data.DataLoader(train_dataset,batch_size=1,shuffle=True,drop_last=True)
    test_loader = torch.utils.data.DataLoader(test_dataset,batch_size=1,drop_last=True)

    model = CNN()
    num_epochs = 10
    optimizer = torch.optim.Adam(model.parameters(),lr=0.001)
    criterion = torch.nn.MSELoss()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model.to(device)
    for epoch in tqdm(range(0, num_epochs)):
        train(epoch, model, optimizer, criterion, train_loader, device)
        test(model, optimizer, criterion, test_loader, device)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('n_tensors', type=int, help="How many tensors to train on")
    parser.add_argument('-p',help='Relative path of evals and fens directories', default='./')
    args = parser.parse_args()
    n_tensors = args.n_tensors
    path = args.p
    main(n_tensors,path)


