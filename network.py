"""Network architecture base on alphazero paper for value head"""
from torch import nn

class BasicLayer(nn.Module):

    def __init__(self, in_planes, planes):
        super().__init__()
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=3, bias=False, padding=1)
        self.bn1 = nn.BatchNorm2d(planes)


    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)

        return out

class ConvLayer(nn.Module):

    def __init__(self, in_planes, planes):
        super().__init__()
        self.conv1 = BasicLayer(in_planes, planes)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        out = self.conv1(x)
        out = self.relu(out)

        return out



class ResLayer(nn.Module):

    def __init__(self, planes):
        super().__init__()
        self.conv1 = BasicLayer(planes, planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = BasicLayer(planes, planes)

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.relu(out)
        out = self.conv2(out)

        out += identity
        out = self.relu(out)

        return out


class ResBlock(nn.Module):

    def __init__(self, planes, nblocks=1):
        super().__init__()
        layers = [ResLayer(planes) for _ in range(nblocks+1)]
        self.residual_layers = nn.Sequential(*layers)

    def forward(self, x):
        out = self.residual_layers(x)

        return out



class ValueHead(nn.Module):

    def __init__(self, planes):
        super().__init__()
        self.conv1 = nn.Conv2d(planes, 1, kernel_size=1, stride=1, bias=False)
        self.bn1 = nn.BatchNorm2d(1)
        self.relu = nn.ReLU(inplace=True)

        self.fc1 = nn.Linear(64,100)
        self.fc2 = nn.Linear(100,1)
        self.tanh = nn.Tanh()

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = nn.Flatten()(out)

        out = self.fc1(out)
        out = self.relu(out)
        out = self.fc2(out)
        out = self.tanh(out)

        return out



class Network(nn.Module):

    def __init__(self, in_planes, planes, n_blocks=1):
        super().__init__()
        self.conv = ConvLayer(in_planes,planes)
        self.resblock = ResBlock(planes, n_blocks)
        self.valuehead = ValueHead(planes)


    def forward(self, x):
        out = self.conv(x)
        out = self.resblock(out)
        out = self.valuehead(out)

        return out
