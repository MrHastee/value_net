import csv
import chess
import torch
from itertools import islice
from tqdm import tqdm

@profile
def main(filename, out_fens, out_evals, num_lines=1000, start_line = 0):
    """Generator pipeline for processing csv of fens and evaluations"""
    fens = get_data(filename, num_lines, start_line)
    boards = transform_fens(fens)
    save_data(boards, out_fens, out_evals)

def get_data(filename, num_lines, start_line):
    """Load fen,evaluation pairs in lazy manner.
    Star at start_line and do num_lines"""
    with open(filename, 'r') as f:
        next(f)
        for fen, evaluation in islice(csv.reader(f), start_line, num_lines):
            yield fen, evaluation

def transform_fens(fen_evals, mate_eval=10_000):
    """Map fen to 8x8x8 tensor from here https://github.com/arjangroen/RLC
    similar ideas elsewhere, AlphaZero e.g.
    #TODO don't use board, maybe it is not very slow without it"""
    mapper = {
                "p":0,
                "r":1,
                "n":2,
                "b":3,
                "q":4,
                "k":5,
                "P":0,
                "R":1,
                "N":2,
                "B":3,
                "Q":4,
                "K":5,
                }

    for fen,ev in fen_evals:
        board = chess.Board(fen)
        layer_board = torch.zeros(size=(8, 8, 8))
        for i in range(64):
            row = i // 8
            col = i % 8
            piece = board.piece_at(i)
            if piece == None:
                continue
            elif piece.symbol().isupper():
                sign = 1
            else:
                sign = -1
            layer = mapper[piece.symbol()]
            layer_board[layer, row, col] = sign
            layer_board[6, :, :] = 1 / board.fullmove_number
        if board.turn:
            layer_board[6, 0, :] = 1
        else:
            layer_board[6, 0, :] = -1
        if board.can_claim_draw():
            layer_board[7, :, :] = 1
        else:
            layer_board[7, :, :] = -1
        if '#' in ev:
            ev = mate_eval if '-' not in ev else -mate_eval
        try:
            ev = torch.tensor(int(ev))
        except ValueError as e:
            # let consumer handle the exception
            yield None,None,e
        yield layer_board,ev,None

def save_data(data,out_fens,out_evals):
    """Save data in tensor form, in out_fens and out_evals directories in seperate files"""
    for idx,(input,label,exception) in enumerate(data):
        current_dir = Path.cwd()
        # here catch exceptions raised by processors
        if not exception:
            save_path_fen = current_dir / f"{out_fens}"
            save_path_eval = current_dir / f"{out_evals}"
            torch.save(input, save_path_fen / f'id-{idx}.pt')
            torch.save(label,save_path_eval / f'id-{idx}.pt')
        else:
            error_path = current_dir / "errors" / f"{idx}"
            Path(error_path).mkdir(parents=True, exist_ok=True)


if __name__ == '__main__':
    import sys
    from pathlib import Path
    out_fens = 'fens'
    out_evals = 'evals'
    num_lines = 1000
    if len(sys.argv) > 1:
        num_lines = int(sys.argv[1])
    print(f'{num_lines=}')
    Path("errors").mkdir(parents=True, exist_ok=True)
    Path(f"{out_fens}").mkdir(parents=True, exist_ok=True)
    Path(f"{out_evals}").mkdir(parents=True, exist_ok=True)
    main('chessData.csv', out_fens, out_evals, num_lines)
